package com.pligor.mytwilio

import com.pligor.generic.HexHelper
import com.ning.http.client.Realm
import java.net.URLEncoder
import com.twilio.sdk.{TwilioRestException, TwilioRestClient}
import com.twilio.sdk.resource.instance.Sms
import scala.collection.JavaConversions._
import scala.concurrent.duration._
import scala.concurrent.Future
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat
import play.api.libs.json.JsString
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.ws.WS
import play.api.libs.ws.WS.WSRequestHolder

object TwilioClient {
  private val trial = false;

  val timeout: Duration = 10.seconds;

  val phoneNumberFormat = PhoneNumberFormat.E164;

  private val trialPrefix = "Sent from your Twilio trial account - ";

  val successSentStatus = "queued"

  val invalidSentStatus = "nonQueued"

  private val maxAvailLengthGSM = 160

  //I do not believe this is necessary if we use just regular text inside messages
  /*private val maxAvailLengthNonGSM = 70

  private def getMaxAvailLength(smsString: String) = {
    maxAvailLengthNonGSM
  }

  def maxTextLength(smsString: String) = if (trial) {
    getMaxAvailLength(smsString) - trialPrefix.length
  } else {
    getMaxAvailLength(smsString)
  }*/
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class TwilioClient(val accountSid: String,
                   val authToken: String,
                   private val primaryNumber: String,
                   private val primaryRegion: String) {
  require(HexHelper.containsOnly(accountSid))

  require(HexHelper.containsOnly(authToken))

  val primaryPhoneNumber: PhoneNumber = PhoneNumberUtil.getInstance().parse(primaryNumber, primaryRegion);

  val primaryNumberDefaultFormat: String = PhoneNumberUtil.getInstance().format(primaryPhoneNumber, TwilioClient.phoneNumberFormat);

  def trySendSms(fromNumber: PhoneNumber, toNumber: PhoneNumber, body: String): Boolean = {
    workWithSms(sendSMS(fromNumber = fromNumber, toNumber = toNumber, body = body))
  }

  def trySendSms(fromNumber: String, toNumber: String, body: String): Boolean = {
    workWithSms(sendSMS(fromNumber = fromNumber, toNumber = toNumber, body = body))
  }

  private def workWithSms(sms: => Sms): Boolean = {
    try {
      sms.getStatus == TwilioClient.successSentStatus
    } catch {
      case e: TwilioRestException => false
    }
  }

  /**
   * @param body truncated to first 160 characters
   * @return
   */
  def sendSMS(fromNumber: PhoneNumber, toNumber: PhoneNumber, body: String): Sms = {
    val client = new TwilioRestClient(accountSid, authToken)
    val mainAccount = client.getAccount
    val smsFactory = mainAccount.getSmsFactory
    val util = PhoneNumberUtil.getInstance()
    val smsParams = Map[String, String](
      "From" -> util.format(fromNumber, TwilioClient.phoneNumberFormat),
      "To" -> util.format(toNumber, TwilioClient.phoneNumberFormat),
      "Body" -> body.take(TwilioClient.maxAvailLengthGSM)
    )
    smsFactory.create(smsParams)
  }

  def sendSMS(fromNumber: String, toNumber: String, body: String): Sms = {
    val client = new TwilioRestClient(accountSid, authToken)

    val mainAccount = client.getAccount

    val smsFactory = mainAccount.getSmsFactory

    val smsParams = Map[String, String](
      "From" -> fromNumber,
      "To" -> toNumber,
      "Body" -> body.take(TwilioClient.maxAvailLengthGSM)
    )

    smsFactory.create(smsParams)
  }

  /**
   * @deprecated
   * @param fromNumber sender
   * @param toNumber receiver
   * @param body message text
   * @return the status
   */
  def sendSMS_old(fromNumber: PhoneNumber, toNumber: PhoneNumber, body: String): Future[String] = {
    val wsRequestHolder: WS.WSRequestHolder = WSRequestHolder(
      url = "https://api.twilio.com/2010-04-01/Accounts/" + accountSid + "/SMS/Messages.json",
      headers = Map.empty[String, Seq[String]],
      queryString = Map.empty[String, Seq[String]],
      calc = None,
      auth = Some((accountSid, authToken, Realm.AuthScheme.BASIC)),

      followRedirects = None, //???
      timeout = None, //???
      virtualHost = None //???
    );

    val util = PhoneNumberUtil.getInstance();

    val fromStr = util.format(fromNumber, TwilioClient.phoneNumberFormat);
    val toStr = util.format(toNumber, TwilioClient.phoneNumberFormat);

    val request = Map[String, Seq[String]](
      "From" -> Seq(URLEncoder.encode(fromStr, "UTF-8")),
      "To" -> Seq(URLEncoder.encode(toStr, "UTF-8")),
      "Body" -> Seq(URLEncoder.encode(body, "UTF-8"))
    );

    val promiseResponse = wsRequestHolder.post(request);

    promiseResponse.map {
      response => {
        val json = response.json;
        json \ "status" match {
          case x: JsString => x.value;
          case _ => throw new Exception("I did not receive the status!");
        }
      }
    }
  }
}
