This helper class is useful for sending messages using twilio service.

###Dependencies###
* Play 2
* com.google.i18n.phonenumbers
* com.pligor.generic
* com.twilio.sdk

Used in the following projects:

* [bman](http://bman.co)
* [Fcarrier](http://facebook.com/FcarrierApp)